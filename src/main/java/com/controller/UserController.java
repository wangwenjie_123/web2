package com.controller;

import com.pojo.TbUser;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author wwj
 * @date 2020/12/29 0029 12:42
 */

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;


    @RequestMapping("/query")
    public String goUserlit(TbUser tbUser) {
        List<TbUser> userList = userService.queryUserList(tbUser);
        //测试用来测试的和你的
        return "user_list";
    }

    @RequestMapping("/goadd")
    public String goUser() {

        return "reuser_add";
    }

    @RequestMapping("/add")
    public String Useradd(TbUser tbUser) {
        int user = userService.UserAdd(tbUser);

        return "redirect:reuser_list";
    }
}
