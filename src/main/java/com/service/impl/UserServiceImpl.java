package com.service.impl;

import com.dao.UserMapper;
import com.pojo.TbUser;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wwj
 * @date 2020/12/29 0029 19:04
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userDao;

    @Override
    public List<TbUser> queryUserList(TbUser user) {
        return userDao.queryUserList(user);
    }

    @Override
    public int UserAdd(TbUser user) {
        return userDao.UserAdd(user);
    }
}
