package com.service;

import com.pojo.TbUser;

import java.util.List;

/**
 * @author wwj
 * @date 2020/12/29 0029 19:03
 */
public interface UserService {
    public List<TbUser> queryUserList(TbUser user);

    public int UserAdd(TbUser user);
}
