package com.util;

import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

/**
 * @author wwj
 * @date 2021/1/4 0004 17:12
 * returnvalue代表返回结果
 */
public class AfterLog implements AfterReturningAdvice {
    @Override
    public void afterReturning(Object returnvalue, Method method, Object[] objects, Object o1) throws Throwable {
        System.out.println("-----------" + method.getName() + "返回结果为" + returnvalue + "-------------------------");
    }
}
