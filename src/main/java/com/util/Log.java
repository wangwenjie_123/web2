package com.util;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * @author wwj
 * @date 2021/1/4 0004 16:43
 */
public class Log implements MethodBeforeAdvice {
    /*
     * method 执行方法
     * args 参数
     * target 目标对象
     * */


    @Override
    public void before(Method method, Object[] objects, Object target) throws Throwable {
        System.out.println("---------------------------" + target.getClass().getName() + "的" + method.getName() + "被执行了----------------------------");

    }
}

