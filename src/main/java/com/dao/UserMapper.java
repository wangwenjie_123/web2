package com.dao;

import com.pojo.TbUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author wwj
 * @date 2020/12/29 0029 19:03
 */
@Mapper
public interface UserMapper {
    public List<TbUser> queryUserList(TbUser user);

    public int UserAdd(TbUser user);
}
